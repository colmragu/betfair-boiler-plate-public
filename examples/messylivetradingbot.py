import csv
import sqlite3
import time
import datetime
from operator import itemgetter
import signal
import threading
import bfpy.bfclient as bfclient
import bfpy
import operator
from sim import tools

def main():
	print "main"
        global go
	global count
	global price
	global activemarkets
	global markets
	count=0
	go=1
	markets=[]
        Thread_login=threading.Thread(target=login, name='login')
        Thread_login.daemon=True
        Thread_login.start()
	time.sleep(5)	
	init()
	activemarkets=[]
	slopelaydraw=0
	slopebackdraw=0
        Thread_getmarkets=threading.Thread(target=get_markets, name='getmarkets')
        Thread_getmarkets.daemon=True
        Thread_getmarkets.start()
	while go:
		for index, market in enumerate(markets):
			backprice=0
			slopebackdraw=0
			momentumback=0
			slopelaydraw=0
			momentumlay=0
			price=0
			try:
	                	price = bf.getMarketPricesCompressed(bfpy.ExchangeUK, marketId=market)		
			except Exception, e:
                        	print ('failed to get market: Error: %s' % str(e))
				break
			price = price.marketPrices
			do_throttle()
			#get slope


			conn = sqlite3.connect('data.db')
			c = conn.cursor()
               	        c.execute("SELECT backdraw,laydraw,currenttime FROM SoccerMatchOdds WHERE marketId==? ORDER BY currenttime DESC LIMIT 1 ",(market,))
			rows=c.fetchone()	
			if (rows!=None and len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToLay)>0):
				now=datetime.datetime.now()
			 	slopelaydraw=tools.slope([price.runnerPrices[2].bestPricesToLay[0].price,now.strftime("%Y-%m-%d %H:%M:%S.%f")],[rows[1],rows[2]])
			else: 
				slopeladydraw=0
			if rows!=None and len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToBack)>0 : 
				backprice=price.runnerPrices[2].bestPricesToBack[0].price
				slopebackdraw=tools.slope([backprice,now.strftime("%Y-%m-%d %H:%M:%S.%f")],[rows[0],rows[2]])
			else:
				backprice=0
				slopebackdraw=0
			c.close()

                        conn = sqlite3.connect('data.db')
        	        c = conn.cursor()
                       	c.execute("SELECT slopebackdraw FROM SoccerMatchOdds WHERE marketId==? and slopebackdraw!=0 ORDER BY currenttime DESC LIMIT 2",(market,))
       		        rows=c.fetchall()
                       	if len(rows)>1:
                       		momentumback=tools.momentum([slopebackdraw,rows[0][0],rows[1][0]])
       			else:
				momentumback=0
	               	c.execute("SELECT slopelaydraw FROM SoccerMatchOdds WHERE marketId==? and slopelaydraw!=0  ORDER BY currenttime DESC LIMIT 2",(market,))	
                       	rows=c.fetchall()	
                        if len(rows)>1:
           	           	momentumlay=tools.momentum([slopelaydraw,rows[0][0],rows[1][0]])
			else:
               	                momentumlay=0                       	       
			c.close()
			writedb(price, slopelaydraw, slopebackdraw)
			if len(activemarkets)<5 and backprice<1.5 and slopebackdraw<-0.0299 and momentumback<-0.009  and slopelaydraw<-0.0299 and momentumlay<-0.009 and price.marketId not in activemarket:
				print "backing market %d @ %f" %(price.marketId, backprice)
				activemarkets.append(price.marketId)
#				success=placebet(price.runnerPrices[2].selectionId, backprice, 2, 'B', price.marketId )
                        if price.marketStatus=="CLOSED":

                                markets.pop(index)
                                #print "removed %d from market tested because game finished"%market

                        	for index, activemarket in enumerate(activemarkets) :
					if price.marketId == activemarket:
                                		activemarkets.pop(index)
                                #	print "removed %d from active markets because game finished"%market								
	return 0
def do_throttle():
        """return only when it is safe to send another data request"""	  
        time.sleep(1.5)
	
def init():
	global bf
	initdb()



	
def login():
	global bf
	lock = threading.Lock()
	while go:
		lock.acquire()

		try:
			bf = bfclient.BfClient(fullDirect=True)
		except Exception, e: 
			print "could not create client %s" % str(e)
        	try:
                	resp = bf.login(username="colmragu", password="Not my real password")#rember to set your password here
			print "log in success"
	        except Exception, e:
        	        print ('Login fail Exiting: Error: %s' % str(e))
		lock.release()

		time.sleep(86400)

	
def get_markets():
        global bf
	global markets
	global go
	timewindow=7.5
	"""returns a list of markets or an error string"""
 	while go: 
        	fromDate = datetime.datetime.now()
        	toDate = fromDate + datetime.timedelta(hours=timewindow)
       		listmarkets=[] 
		try:
        		bfresp = bf.getAllMarkets(bfpy.ExchangeUK, locale='en', eventTypeIds=[1], fromDate=fromDate, toDate=toDate)
        	except Exception, e:
                	print ('Could not get markets: Error: %s' % str(e)) 
			break
	
#		markets=bfresp.marketData

        	tmpmarket=filter(lambda x: x.marketName == 'Match Odds' and x.turningInPlay, bfresp.marketData)
	        
		if len(markets)<1:
			for market in tmpmarket:
				#print "added %d to markets monitored at init" %market.marketId
				markets.append(market.marketId) 
				
		for market in tmpmarket:
			if market.marketId not in markets:
#				print "added %d to marktes montioried" %market.marketId 
				markets.append(market.marketId)
        	if len(markets) == 0:
                	# no markets found...
              	  	#s = 'No markets found. Sleeping for 30 minutes...'
                	#print s
               	 	#time.sleep(1900) # bandwidth saver!
			#print "sleeping 1900"
			time.sleep(450)#       return markets
		#print "sleeping60 "
#	 	print markets
		time.sleep(450)
		
def initdb():
        conn = sqlite3.connect('data.db')#%(datetime.datetime.now())
        #print "wrtieing to db"
        c = conn.cursor()

        # Create tables
        c.execute("CREATE TABLE if not exists Key (MarketId, EventId)")
        c.execute("CREATE TABLE if not exists SoccerMatchOdds (bspMarket,currencyCode,delay,discountAllowed,lastRefresh,marketBaseRate,marketId,marketInfo,marketStatus,numberOfWinners,removedRunners,BackHome,BackHomeAmount,LayHome,LayHomeAmount,BackAway,BackAwayAmount,LayAway,LayAwayAmount,BackDraw,BackDrawAmount,LayDraw,LayDrawAmount,timeinplay, currenttime,slopelaydraw,slopebackdraw)")
	conn.commit() 
        
def writedb(price, slopelaydraw, slopebackdraw):
	conn = sqlite3.connect('data.db')#%(datetime.datetime.now())
	#print "wrtieing to db"
	c = conn.cursor()
	
	c.execute("SELECT currencyCode FROM SoccerMatchOdds WHERE  marketId=%s"%price.marketId)
	rows = c.fetchall()

	if not rows:

		c.execute("INSERT INTO Key VALUES(%s,7)"%price.marketId)


	timeinplay=0

	currenttime=datetime.datetime.now()
	
        if (len(price.runnerPrices)>0 and len(price.runnerPrices[0].bestPricesToBack)>0 ):
		BackHome=price.runnerPrices[0].bestPricesToBack[0].price
		BackHomeAmount=price.runnerPrices[0].bestPricesToBack[0].amountAvailable
        else:
                BackHome=1
		BackHomeAmount=0

	if (len(price.runnerPrices)>0 and len(price.runnerPrices[0].bestPricesToLay)>0):
                LayHome=price.runnerPrices[0].bestPricesToLay[0].price
                LayHomeAmount=price.runnerPrices[0].bestPricesToLay[0].amountAvailable
        else:
                LayHome=1
                LayHomeAmount=0
        if (len(price.runnerPrices)>1 and len(price.runnerPrices[1].bestPricesToBack)>0):
                BackAway=price.runnerPrices[1].bestPricesToBack[0].price
                BackAwayAmount=price.runnerPrices[1].bestPricesToBack[0].amountAvailable
        else:
                BackAway=1
                BackAwayAmount=0

        if (len(price.runnerPrices)>1 and len(price.runnerPrices[1].bestPricesToLay)>0):
                LayAway=price.runnerPrices[1].bestPricesToLay[0].price
                LayAwayAmount=price.runnerPrices[1].bestPricesToLay[0].amountAvailable
        else:
                LayAway=1
                LayAwayAmount=0
        if (len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToBack)>0):
                BackDraw=price.runnerPrices[2].bestPricesToBack[0].price
                BackDrawAmount=price.runnerPrices[2].bestPricesToBack[0].amountAvailable
        else:
                BackDraw=1
                BackDrawAmount=0

        if (len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToLay)>0):
                LayDraw=price.runnerPrices[2].bestPricesToLay[0].price
                LayDrawAmount=price.runnerPrices[2].bestPricesToLay[0].amountAvailable
        else:
                LayDraw=1
                LayDrawAmount=0

	
	try:
	        c.execute("INSERT INTO SoccerMatchOdds VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [price.bspMarket, price.currencyCode, price.delay,price.discountAllowed,price.lastRefresh,price.marketBaseRate,price.marketId,price.marketInfo,price.marketStatus,price.numberOfWinners,price.removedRunners,BackHome, BackHomeAmount,LayHome,LayHomeAmount,BackAway,BackAwayAmount,LayAway, LayAwayAmount,BackDraw, BackDrawAmount,LayDraw,LayDrawAmount,timeinplay,currenttime,slopelaydraw,slopebackdraw])
	except Exception, e:
		print "error writing database %s"%str(e)
		var=0
	conn.commit()
	c.close()

def WriteLog(infile, data):
    	logfile=open(infile, 'a')
   	fieldnames=data.keys()
   	log = csv.DictWriter(logfile, fieldnames=fieldnames,  delimiter=',',quotechar=',', quoting=csv.QUOTE_MINIMAL)
   	log.writerow(data)
    	logfile.close()
	return 1

def placebet(selectionId, price, size, Type, marketId ):
	global bf
        placeBet = bf.createPlaceBets()
        placeBet.asianLineId = 0
        placeBet.selectionId = selectionId
        placeBet.price = price
        placeBet.size = size
        placeBet.bspLiability = 0.0
        placeBet.betType = Type
        placeBet.betCategoryType = 'E'
        placeBet.betPersistenceType = 'NONE'
        placeBet.marketId =marketId
	try:
        	response = bf.placeBets(bfpy.ExchangeUK, bets=[placeBet])
	except:
#		print "failed to place bet"
		return 0
	return 1
	
    #    print response

        return 1

def laydraw(market):
#	print "staring lay draw"
	#place bet
	global price
	global activemarkets
	print market
	profit=[]
	drawexposure=[]
	steaklaydraw=4
	layprice=market["price"]
	backprice=price.runnerPrices[2].bestPricesToLay[0].price
	while True:

        	market["event"].wait()
		backprice=price.runnerPrices[2].bestPricesToBack[0].price
	#	print "checking odds %f %d"%(backprice,price.marketId)
		if backprice>4.2:
 	    		#make profit
	                #print "maeking profit%d"%element[3]
        	        steakbackdraw=((layprice/backprice)*steaklaydraw)
	                profit.append((steaklaydraw-steakbackdraw))
        	        drawexposure.append(steakbackdraw)
			#placebet(price.runnerPrices[2].selectionId, backprice, steakbackdraw, 'B', price.marketId )
			print profit
			for index,amarket in enumerate(activemarkets):
				if amarket["marketId"]==market["marketId"]:
					activemarkets.pop(index)
        		return 1        
        	if backprice<1:
	                #make loss
        	        #print "making loss"
                	steakbackdraw=((layprice/backprice)*steaklaydraw)
	                drawexposure.append(steakbackdraw)
        	        profit.append((steaklaydraw-steakbackdraw))
			print profit
			#placebet(price.runnerPrices[2].selectionId, backprice, steakbackdraw, 'B', price.marketId )
                        for index,amarket in enumerate(activemarkets):
                                if amarket["marketId"]==market["marketId"]:
                                        activemarkets.pop(index)

			return 0
		#wait for next price object	
		market["event"].clear()	

def scalp(market):
	print "scalping"
	global activemarkets
        print market
        profit=[]  
        drawexposure=[]
        steaklaydraw=4
        backprice=market["price"]
        layprice=price.runnerPrices[2].bestPricesToLay[0].price
	#placebet
	while go: 
		market["event"].wait()
		if layprice< backprice:
			print "profit %d"%market["marketId"]

if __name__ == "__main__":
    main() 
