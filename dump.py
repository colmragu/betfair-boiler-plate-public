import csv
import sqlite3
import time
import datetime
from operator import itemgetter
import signal
import threading
import bfpy.bfclient as bfclient
import bfpy
import operator
from sim import tools

def main():
	# dumps data from betfair into data.db
	print "main"
        global go
	global count
	global price
	global activemarkets
	global markets
	count=0
	go=1
	markets=[]
        Thread_login=threading.Thread(target=login, name='login')
        Thread_login.daemon=True
        Thread_login.start()
	time.sleep(5)	
	init()
	activemarkets=[]

        Thread_getmarkets=threading.Thread(target=get_markets, name='getmarkets')
        Thread_getmarkets.daemon=True
        Thread_getmarkets.start()

	while go:
		for index, market in enumerate(markets):
			try:
	                	price = bf.getMarketPricesCompressed(bfpy.ExchangeUK, marketId=market)		
			except Exception, e:
                        	print ('failed to get market: Error: %s' % str(e))
				break
			price = price.marketPrices
			do_throttle()
			writedb(price, slopelaydraw, slopebackdraw)
	
	return 0
def do_throttle():
	# enforces betfair requiest limit. Needs to be updated so that is takes into account requests rather than just sleeping 1.5 seconds
        time.sleep(1.5)
	
def init():
	#init
	global bf
	initdb()	
def login():
	# do logins. call from thread so that it constanly refreshs login.  
	global bf
	lock = threading.Lock()
	while go:
		lock.acquire()

		try:
			bf = bfclient.BfClient(fullDirect=True)
		except Exception, e: 
			print "could not create client %s" % str(e)
        	try:
                	resp = bf.login(username="colmragu", password="Rember to change") # change to your password
			print "log in success"
	        except Exception, e:
        	        print ('Login fail Exiting: Error: %s' % str(e))
		lock.release()

		time.sleep(86400)

	
def get_markets():
	# gets a list of the marketIds of all active markets on betfair (one for each football match Win,Lose, Draw. See betfair Api d		documentaion) 
        global bf
	global markets
	global go
	timewindow=7.5
	"""returns a list of markets or an error string"""
 	while go: 
        	fromDate = datetime.datetime.now()
        	toDate = fromDate + datetime.timedelta(hours=timewindow)
       		listmarkets=[] 
		try:
        		bfresp = bf.getAllMarkets(bfpy.ExchangeUK, locale='en', eventTypeIds=[1], fromDate=fromDate, toDate=toDate)
        	except Exception, e:
                	print ('Could not get markets: Error: %s' % str(e)) 
			break
        	tmpmarket=filter(lambda x: x.marketName == 'Match Odds' and x.turningInPlay, bfresp.marketData)
	       	if len(markets)<1:
			for market in tmpmarket:

				markets.append(market.marketId) 
				
		for market in tmpmarket:
			if market.marketId not in markets:

				markets.append(market.marketId)
        	if len(markets) == 0:
			time.sleep(450)#       return markets
		time.sleep(450)
		
def initdb():
        # initiated databse
	conn = sqlite3.connect('data.db') 
        
        c = conn.cursor()

        # Create tables
        c.execute("CREATE TABLE if not exists Key (MarketId, EventId)")
        c.execute("CREATE TABLE if not exists SoccerMatchOdds (bspMarket,currencyCode,delay,discountAllowed,lastRefresh,marketBaseRate,marketId,marketInfo,marketStatus,numberOfWinners,removedRunners,BackHome,BackHomeAmount,LayHome,LayHomeAmount,BackAway,BackAwayAmount,LayAway,LayAwayAmount,BackDraw,BackDrawAmount,LayDraw,LayDrawAmount,timeinplay, currenttime)")
	conn.commit() 
        
def writedb(price, slopelaydraw, slopebackdraw):
	
	conn = sqlite3.connect('data.db') #write to database data.db

	c = conn.cursor()
	
	c.execute("SELECT currencyCode FROM SoccerMatchOdds WHERE  marketId=%s"%price.marketId)
	rows = c.fetchall()

	if not rows:
		# keys is  a table that lists the marketId of all markets that we have data for. 
		c.execute("INSERT INTO Key VALUES(%s,7)"%price.marketId)


	timeinplay=0

	currenttime=datetime.datetime.now()
	# makes sure each of the data entrys exists. If they do not we set values of 1 for odds and 0 for ammouth it is hoped
	# that this will not effect ours sims. Be mindful of this wehn running sims. 	
        if (len(price.runnerPrices)>0 and len(price.runnerPrices[0].bestPricesToBack)>0 ):
		BackHome=price.runnerPrices[0].bestPricesToBack[0].price
		BackHomeAmount=price.runnerPrices[0].bestPricesToBack[0].amountAvailable
        else:
                BackHome=1
		BackHomeAmount=0

	if (len(price.runnerPrices)>0 and len(price.runnerPrices[0].bestPricesToLay)>0):
                LayHome=price.runnerPrices[0].bestPricesToLay[0].price
                LayHomeAmount=price.runnerPrices[0].bestPricesToLay[0].amountAvailable
        else:
                LayHome=1
                LayHomeAmount=0
        if (len(price.runnerPrices)>1 and len(price.runnerPrices[1].bestPricesToBack)>0):
                BackAway=price.runnerPrices[1].bestPricesToBack[0].price
                BackAwayAmount=price.runnerPrices[1].bestPricesToBack[0].amountAvailable
        else:
                BackAway=1
                BackAwayAmount=0

        if (len(price.runnerPrices)>1 and len(price.runnerPrices[1].bestPricesToLay)>0):
                LayAway=price.runnerPrices[1].bestPricesToLay[0].price
                LayAwayAmount=price.runnerPrices[1].bestPricesToLay[0].amountAvailable
        else:
                LayAway=1
                LayAwayAmount=0
        if (len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToBack)>0):
                BackDraw=price.runnerPrices[2].bestPricesToBack[0].price
                BackDrawAmount=price.runnerPrices[2].bestPricesToBack[0].amountAvailable
        else:
                BackDraw=1
                BackDrawAmount=0

        if (len(price.runnerPrices)>2 and len(price.runnerPrices[2].bestPricesToLay)>0):
                LayDraw=price.runnerPrices[2].bestPricesToLay[0].price
                LayDrawAmount=price.runnerPrices[2].bestPricesToLay[0].amountAvailable
        else:
                LayDraw=1
                LayDrawAmount=0

	
	try:
	        c.execute("INSERT INTO SoccerMatchOdds VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [price.bspMarket, price.currencyCode, price.delay,price.discountAllowed,price.lastRefresh,price.marketBaseRate,price.marketId,price.marketInfo,price.marketStatus,price.numberOfWinners,price.removedRunners,BackHome, BackHomeAmount,LayHome,LayHomeAmount,BackAway,BackAwayAmount,LayAway, LayAwayAmount,BackDraw, BackDrawAmount,LayDraw,LayDrawAmount,timeinplay,currenttime])
	except Exception, e:
		print "error writing database %s"%str(e)
		var=0
	conn.commit()
	c.close()

def placebet(selectionId, price, size, Type, marketId ):
	# function to place a bet on betfair use it when you have live trading bots see examples. 
	global bf
        placeBet = bf.createPlaceBets()
        placeBet.asianLineId = 0
        placeBet.selectionId = selectionId
        placeBet.price = price
        placeBet.size = size
        placeBet.bspLiability = 0.0
        placeBet.betType = Type
        placeBet.betCategoryType = 'E'
        placeBet.betPersistenceType = 'NONE'
        placeBet.marketId =marketId
	try:
        	response = bf.placeBets(bfpy.ExchangeUK, bets=[placeBet])
	except:

		return 0
	return 1
	


        return 1
if __name__ == "__main__":
    main() 
