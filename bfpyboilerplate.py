import sqlite3
import datetime
import bfpy

def init():
	conn=sqlite3.connect("9Feb2013.db")
	c=conn.cursor()
	return c
def getmarketlist(price):
	marketlist=[]
	marketlist.append(price)
	return (marketlist)
def grab(time):
	'''Simulates the betfair api. This function returns the market object value that the API returned and was stored at the given 		time. if time is zero then returns the first object grap simulates the return of 
		price = bf.getMarketPricesCompressed(bfpy.ExchangeUK, marketId=market)'''
	c=init()
	if len(time)<23:
		time+=str('.')
	while len(time)<23:
		time+=str(0)
	try:
                time=datetime.datetime.strptime(time[:23],'%Y-%m-%d %H:%M:%S.%f')
	except:
		print "setting time to fist time in database"
		c.execute("SELECT lastRefresh FROM SoccerMatchOdds ORDER BY lastRefresh ASC LIMIT 1")
        	time=c.fetchone()
	        time=datetime.datetime.strptime(time[0],'%Y-%m-%d %H:%M:%S.%f') 
        c.execute("SELECT * FROM SoccerMatchOdds WHERE lastRefresh>?  ORDER BY lastRefresh ASC  LIMIT 1",[time.strftime('%Y-%m-%d %H:%M:%S.%f')])
	row=c.fetchone()		
	if row == None:
		return 0
	row=ParseMarketPrices(int(row[6]), str(row[1]),str(row[7]), str(row[4]), str(row[9]), str(row[7]), str(row[3]), str(row[5]),str(row[4]), str(row[10]), int(row[0]), float(row[11]), float(row[12]), float(row[13]), float(row[14]), float(row[15]), float(row[16]), float(row[17]), float(row[18]), float(row[19]), float(row[20]), float(row[21]), float(row[22]) )
	c.close()
	return row


def ParseMarketPrices(marketId, currencyCode, marketStatus, delay, numberOfwinners, marketInfo, discountAllowed, marketBaseRate, lastRefresh,removedRunners, bspMarket, BackHome, BackHomeAmount,LayHome,LayHomeAmount,BackAway, BackAwayAmount, LayAway, LayAwayAmount,BackDraw,BackDrawAmount,LayDraw,LayDrawAmount):
	''' creates a bfpy object from the bassed data'''
	marketPrices = bfpy.bfutil.Factory.MarketPrices()
        marketPrices.marketId = marketId
        marketPrices.currencyCode = currencyCode
        marketPrices.marketStatus = marketStatus
        marketPrices.delay = delay
        marketPrices.numberOfWinners = numberOfwinners
        marketPrices.marketInfo = marketInfo
        marketPrices.discountAllowed = discountAllowed
        marketPrices.marketBaseRate = marketBaseRate
        marketPrices.lastRefresh = lastRefresh
        marketPrices.removedRunners = removedRunners
        marketPrices.bspMarket = bspMarket
	marketPrices.runnerPrices=list()
	runner = bfpy.bfutil.Factory.RunnerPrices()
        runner.bestPricesToBack = list()
        runner.bestPricesToLay = list()
        # Assign the header fields
        runner.selectionId = 0
        runner.sortOrder = 0
        runner.totalAmountMatched = 0
        # The following 3 fields may be empty, therefore raising a exception
        runner.lastPriceMatched = 0
        runner.handicap = 0
        runner.reductionFactor = 0
        runner.bestPricesToBack.append(ParseRunnerPrices([BackHome,BackHomeAmount,1,1]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
	runner.bestPricesToLay.append(ParseRunnerPrices([LayHome,LayHomeAmount,1,1]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
        marketPrices.runnerPrices.append(runner)

	runner = bfpy.bfutil.Factory.RunnerPrices()
        runner.bestPricesToBack = list()
        runner.bestPricesToLay = list()
        # Assign the header fields
        runner.selectionId = 0
        runner.sortOrder = 0
        runner.totalAmountMatched = 0
        # The following 3 fields may be empty, therefore raising a exception
        runner.lastPriceMatched = 0
        runner.handicap = 0
        runner.reductionFactor = 0 
        runner.bestPricesToBack.append(ParseRunnerPrices([BackAway,BackAwayAmount,1,1]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToLay.append(ParseRunnerPrices([LayAway,LayAwayAmount,1,1]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
  	marketPrices.runnerPrices.append(runner)


        runner = bfpy.bfutil.Factory.RunnerPrices()
        runner.bestPricesToBack = list()
        runner.bestPricesToLay = list()
        # Assign the header fields
        runner.selectionId = 0
        runner.sortOrder = 0
        runner.totalAmountMatched = 0
        # The following 3 fields may be empty, therefore raising a exception
        runner.lastPriceMatched = 0
        runner.handicap = 0
        runner.reductionFactor = 0
        runner.bestPricesToBack.append(ParseRunnerPrices([BackDraw,BackDrawAmount,1,1]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToBack.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToLay.append(ParseRunnerPrices([LayDraw,LayDrawAmount,1,1]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
        runner.bestPricesToLay.append(ParseRunnerPrices([0,0,0,0]))
        marketPrices.runnerPrices.append(runner)

	return marketPrices

def ParseRunnerPrices(parts):
        price = bfpy.bfutil.Factory.Price()
        # Split the price in its constituent parts
        # parts = p_price.split('~')
        # Ideally the string is already a list
        price.price = float(parts[0])
        price.amountAvailable = float(parts[1])
        price.betType = parts[2]
        price.depth = int(parts[3])
	return price

def dumprows(marketId):
	global c
	c = init()
        for element in c.execute("SELECT delay,BackDraw,LayDraw,marketId,currenttime FROM SoccerMatchOdds WHERE marketId==? ORDER BY lastRefresh",marketId):
        	print element
	c.close()	
       
                       
                      
